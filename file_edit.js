
(function($) {

	Drupal.fileEdit || (Drupal.fileEdit = {});



	/**
	 * Public, overridable file_edit functions.
	 */

	Drupal.fileEdit.onClicked = function(e) {
		e.preventDefault();

		var
			$this = $(this),
			$file = $this.parent().parent().find('[name*="[fid]"]'),
			fid = $file.val(),
			path = Drupal.settings.basePath + 'file/' + fid + '/edit?iframe&file_edit'
		;
		Drupal.fileEdit.openModal(path);
	};

	// Extend dialog options.
	Drupal.fileEdit.modalOptions = function(options, context) {
		return options;
	};

	// The handler that opens the Views grid in a modal.
	Drupal.fileEdit.openModal = function(path, context) {
		// Prep options.
		var
			height = 0.8 * document.documentElement.clientHeight,
			width = 0.8 * document.documentElement.clientWidth,
			saveText = Drupal.t('Save'),
			cancelText = Drupal.t('Cancel'),
			buttons = {};
		buttons[saveText] = function(e) {
			var
				$modal = Drupal.fileEdit.$modal,
				iframe = $modal.find('iframe')[0],
				iframeWindow = iframe.contentWindow,
				iframeDocument = iframe.contentDocument,
				$submit = iframeWindow.jQuery('#edit-submit', iframeDocument);

			$submit.trigger('mousedown');
		};
		buttons[cancelText] = function(e) {
			var $modal = Drupal.fileEdit.$modal;
			$modal.dialog('close');
		};

		// Create and extend options.
		var options = {
			"dialogClass": 'file-edit-modal',
			"width": width,
			"height": height,
			"draggable": false,
			"modal": true,
			"overlay": {
				"backgroundColor": '#000000',
				"opacity": 0.4,
			},
			"resizable": false,
			"buttons": buttons,
			"titlebar": false,
		};
		options = Drupal.fileEdit.modalOptions(options, context);

		// Create modal.
		var $modal = $('<div><iframe width="100%" height="100%" src="' + path + '"></iframe></div>');
		$('body').append($modal);
		$modal.dialog(options);

		// Remove title bar and reset height.
		if (!options.titlebar) {
			$modal.closest(".ui-dialog").find(".ui-dialog-titlebar").remove();
			$modal.dialog('option', 'height', height);
		}

		return Drupal.fileEdit.$modal = $modal;
	};



	/**
	 * Drupal behaviors.
	 */

	Drupal.behaviors.fileEdit = {
		attach: function(context, settings) {
			var buttons = $('.field-type-image .form-submit, .field-type-file .form-submit')
				.filter('[name*="remove_button"]')
				.filter(':not(.file-edit-processed)')
			;
			buttons.each(function() {
				var
					$btn = $(this),
					$editButton = $('<a>')
						.text(Drupal.t('Edit'))
						.attr('href', '#')
						.addClass('button')
						.addClass('file-edit-button')
						.click(Drupal.fileEdit.onClicked)
				;
				$editButton.insertAfter($btn);

				$btn.addClass('file-edit-processed');
			});
		}
	};

	if (Drupal.ajax) {
		Drupal.ajax.prototype.commands.file_edit_close = function(ajax) {
			parent.Drupal.fileEdit.$modal.dialog('close');
		};
	}

})(jQuery);
